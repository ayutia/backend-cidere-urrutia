import asyncpg
from fastapi.responses import JSONResponse
from modelos.modelo_tamano import Tamano
from modelos.modelo_rubro import Rubro
from modelos.modelo_empresa import Empresa, Servicios
from modelos.modelo_region import Comuna, Provincia, Region
from fastapi import FastAPI, Query
import asyncio
from sqlalchemy import text
from sqlalchemy.sql import select
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.ext.asyncio.engine import AsyncEngine
from pydantic import ValidationError
from typing import Annotated, List

from config import Settings

app = FastAPI()
settings = Settings()
engine = create_async_engine(settings.asyncpg_url.unicode_string())



#TODO definir las consultas para las query
@app.get("/api/servicios")
async def obtener_proveedores():
    try:
        async with engine.begin() as conn:
            servicios = await conn.execute(
                text("SELECT id_empresa, nombre, info, servicios->'servicios' AS servicios "
                     + "FROM servicios_ofrecidos "
                     + "INNER JOIN empresas ON servicios_ofrecidos.id_empresa = empresas.id"
                    # +"WHERE id_empresa = :id").bindparams(id = IdEmpresa)
                )
            )
            servicios_nuevos = [Servicios(**servicio._asdict()) for servicio in servicios]
            conn.aclose()
            return servicios_nuevos
    except ValidationError as exc:
        return JSONResponse(content={"error": str(exc)}, status_code=500)

@app.get("/api/comuna/{comuna}")
async def obtener_empresas_comuna(comuna :str):
    try:
        async with engine.begin() as conexion:
            print("Entre")
            empresas = await conexion.execute(
                text("SELECT empresas.id AS id, empresas.nombre AS nombre, rut, razon_social, tipo_empresa.nombre AS tipo, direccion "+
                     "FROM empresas INNER JOIN tipo_empresa ON empresas.id_tipo_empresa = tipo_empresa.id "+
                     "INNER JOIN direccion_empresa ON empresas.id = direccion_empresa.id_empresa " 
                     #" WHERE direccion_empresa.id_comuna = (SELECT id FROM comunas WHERE comunas.nombre = '"+ comuna +"')"
                     ))
            await conexion.aclose()
            empresas_nuevas = [Empresa(**empresa._asdict()) for empresa in empresas]
            for emp in empresas_nuevas:
                if emp.nombre != comuna:
                    empresas_nuevas.remove(emp)
            return empresas_nuevas
    except ValidationError as exc:
        print(repr(exc.errors()[0]['type']))
        return JSONResponse(content={"error": str(exc)}, status_code=500)               

#TODO endpoints

@app.get("/api/empresas")
#buscar :str "La busqueda"
#IdRegion:int id de la region
#IdProvincia:int id de la prov
#IdComuna:int id de la prov
#IdRubro:int
#IdSize:int tamaño empresa
async def get_empresas(buscar: Annotated[str | None, Query()] = None,
                       IdComuna: Annotated[list[int] | None, Query()] = None,
                       IdProvincia: Annotated[list[int] | None, Query()] = None,
                       IdRegion: Annotated[list[int] | None, Query()] = None,
                       IdRubro: Annotated[list[int] | None, Query()] = None,
                       IdSize: Annotated[list[int] | None, Query()] = None
                       ):
    try:
        async with engine.begin() as conn:
            if  buscar == None and IdComuna == None and IdProvincia == None and IdRegion == None and IdRubro == None and IdSize == None:
                empresas = await conn.execute(
                    text("SELECT empresas.id AS id, empresas.nombre AS nombre, rut, razon_social, tipo_empresa.nombre AS tipo, direccion "+
                         "FROM empresas INNER JOIN tipo_empresa ON empresas.id_tipo_empresa = tipo_empresa.id "+
                         "INNER JOIN direccion_empresa ON empresas.id = direccion_empresa.id_empresa ")
                )
                await conn.aclose()
                empresas_nue1vas = [Empresa(**empresa._asdict()) for empresa in empresas]
                return empresas_nuevas
            else:
                empresas_nuevas = []
                if buscar != None:
                    empresas = await conn.execute(
                        text(
                        "SELECT empresas.id AS id, empresas.nombre AS nombre, rut, razon_social, tipo_empresa.nombre AS tipo, direccion "
                        + "FROM empresas INNER JOIN tipo_empresa ON empresas.id_tipo_empresa = tipo_empresa.id "
                        + "INNER JOIN direccion_empresa ON empresas.id = direccion_empresa.id_empresa "
                        + "WHERE empresas.nombre ILIKE '"+ buscar+"' "
                        + "OR empresas.info ILIKE '%"+buscar+"%' "
                        
                        # TODO EXPANDIR
                        )
                    )
                    empresas_nuevas.append([Empresa(**empresa._asdict()) for empresa in empresas])
                    
                if IdComuna != None:
                    for q in IdComuna:
                        #do some shit
                        empresas = await conn.execute(
                            text("SELECT empresas.id AS id, empresas.nombre AS nombre, rut, razon_social, tipo_empresa.nombre AS tipo, direccion "+
                            "FROM empresas INNER JOIN tipo_empresa ON empresas.id_tipo_empresa = tipo_empresa.id "+
                            "INNER JOIN direccion_empresa ON empresas.id = direccion_empresa.id_empresa " +
                            "WHERE direccion_empresa.id_comuna =:q_id").bindparams(q_id = q)
                        )
                        empresas_nuevas.append([Empresa(**empresa._asdict()) for empresa in empresas])
                #return {'message':'Entre'}
                await conn.aclose()
                return empresas_nuevas
    except ValidationError as exc:
        return JSONResponse(content={"error": str(exc)}, status_code=500)
    
@app.get("/api/region")
async def get_region():
    try:
        #some code
        async with engine.begin() as conn:
            regiones = await conn.execute(
                text(
                    "SELECT id, nombre FROM regiones"
                )
            )
            regiones_retorno = [Region(**region._asdict()) for region in regiones]
            await conn.aclose()
        return regiones_retorno
    except ValidationError as exc:
        return JSONResponse(content={"error": str(exc)}, status_code=500)
    

@app.get("/api/provincia")
async def get_provincia():
    try:
        #some code
        async with engine.begin() as conn:
            provincias = await conn.execute(
                text(
                    "SELECT id_region, id, nombre FROM provincias"
                )
            )
            provincias_retorno = [Provincia(**provincia._asdict()) for provincia in provincias]
            await conn.aclose()
        return provincias_retorno
    except ValidationError as exc:
        return JSONResponse(content={"error": str(exc)}, status_code=500)
    
@app.get("/api/comuna")
async def get_comuna():
    try:
        #some code
        async with engine.begin() as conn:
            comunas = await conn.execute(
                text(
                    "SELECT id_region, id_provincia, comunas.id AS id, comunas.nombre AS nombre "
                    + "FROM comunas "
                    + "LEFT JOIN provincias ON comunas.id_provincia = provincias.id"
                )
            )
            comunas_retorno = [Comuna(**comuna._asdict()) for comuna in comunas]
            await conn.aclose()
        return comunas_retorno
    except ValidationError as exc:
        return JSONResponse(content={"error": str(exc)}, status_code=500)

@app.get("/api/rubro")
async def get_rubro():
    try:
        #some code
        async with engine.begin() as conn:
            rubros = await conn.execute(
                text(
                    "SELECT id, nombre FROM rubros"
                )
            )
            rubros_retorno = [Rubro(**rubro._asdict()) for rubro in rubros]
            await conn.aclose()
        return rubros_retorno
    except ValidationError as exc:
        return JSONResponse(content={"error": str(exc)}, status_code=500)


@app.get("/api/tamano")
async def get_tamano():
    try:
        #some code
        async with engine.begin() as conn:
            tamanos = await conn.execute(
                text(
                    "SELECT id, nombre FROM tamano_empresa"
                )
            )
            tamanos_retorno = [Tamano(**tamano._asdict()) for tamano in tamanos]
            await conn.aclose()
        return tamanos_retorno
    except ValidationError as exc:
        return JSONResponse(content={"error": str(exc)}, status_code=500)
    

#TODO Buscador de proveedor
"""
id
nombre
info
servicios
"""
@app.get("/api/proveedores")
async def buscar_proveedores(buscar: Annotated[str | None, Query()] = None):
    try:
        #some code
        async with engine.begin() as conn:
            if buscar == None:
                proveedores = await conn.execute(
                    text(
                        "SELECT empresas.id as id_empresa, nombre, info, servicios_ofrecidos.servicios->'servicios' AS servicios "
                     + "FROM empresas "
                     + "INNER JOIN servicios_ofrecidos ON empresas.id = servicios_ofrecidos.id_empresa"
                    )
                )
                proveedores_retorno = [Servicios(**proveedor._asdict()) for proveedor in proveedores]
                await conn.aclose()
            else:
                proveedores = await conn.execute(
                    text(
                        "SELECT empresas.id as id_empresa, nombre, info, servicios_ofrecidos.servicios->'servicios' AS servicios "
                        + "FROM empresas "
                        + "INNER JOIN servicios_ofrecidos ON empresas.id = servicios_ofrecidos.id_empresa "
                        + "WHERE nombre ILIKE '"+buscar+"' "
                        + "OR info ILIKE '%"+buscar+"%' "
                        + "OR servicios ->>'servicios' ILIKE '%"+buscar+"%'"
                    )
                )
                proveedores_retorno = [Servicios(**proveedor._asdict()) for proveedor in proveedores]
                await conn.aclose()
        return proveedores_retorno
    except ValidationError as excError:
        return JSONResponse(content={"error": str(excError)}, status_code=500)
