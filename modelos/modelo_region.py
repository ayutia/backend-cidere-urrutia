from pydantic import BaseModel, Field
from typing import Optional

class Comuna(BaseModel):
    id_region: int
    id_provincia:int
    id:int
    nombre:str
    
class Provincia(BaseModel):
    id_region:int
    id:int
    nombre:str
    
class Region(BaseModel):
    id:int
    nombre:str