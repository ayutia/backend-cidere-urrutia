# 12-11 : Crearemos un objeto aparte de esto y el pydantic lo validara, espero que este enfoque funcione.
from pydantic import BaseModel, Field
from typing import Optional

class Servicios(BaseModel):
    id_empresa: int
    nombre: str
    info: str
    servicios: Optional[list[str]] = None

class Empresa(BaseModel):
    id: int
    nombre: str
    rut: str
    razon_social: str
    tipo: str
    direccion: str
    # Aca faltan campos que describen a la empresa. Algunos son opcionales