import os
from dotenv import load_dotenv
from pydantic import PostgresDsn
from pydantic_settings import BaseSettings

load_dotenv()

class Settings(BaseSettings):
    asyncpg_url : PostgresDsn = os.getenv("SQL_URL")